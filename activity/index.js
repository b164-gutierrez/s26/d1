
//5. Import the http module using the required directive.
const http = require('http');


//6. Create port = 3000
const port = 3000;


//7. createServer

const server = http.createServer((req, res) => {
		
		//9. Create condition
		if(req.url =='/login'){
				res.writeHead(200, {'Content-Type': 'text/plain'});
				res.end('Login Page')
		}
	else {
		res.writeHead(404,{'Content-Type':'text/plain'});
		res.end('Page not found')
	}
})

server.listen(port);

//8. console log
console.log(`Server is Running Successfully:${port}.`)
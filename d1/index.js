					//NODE JS 



// create a basic server application by loading NODE js modules

// module it is a software component or part of a program that contains one or more routines.

// http - hyper text transfer protocol
// http is a module to node that transfer data. it is a set of individual files that contain code to create a component that helps establish data transfer between applications.
// clients(browser) and servers (node js/express) communicate by exchanging individual messages.
// the messages sent by the client, usually a web browser, are called REQUEST.
// the messages sent by the server as an answer are called RESPONSES.

// require() function allows us to use built in modules.
let http = require('http');

// a 'PORT' is a virtual point where network connections start and end.
// each PORT is associated with a specific process or service
// the server will be assigned to port 4000 via the listen (4000) method where the server will listen to any request that are sent to our server

// http://localhost:4000

// we use the response.writeHead() method to set a status code for the response - a 200 means OK
// set the content-type


// response.end() method to end the respond process

http.createServer(function(request, response) {
		response.writeHead(200, {'Content-Type': 'application/json'}); //text/plain or image/jpeg
		response.end('Hello World')

}).listen(4000);

// when server is running, console will print the message:
console.log('Server running at localhost:4000');

// status code
// 100-199 = Informational responses
// 200-299 = Successful responses
// 300-399 = redirection messages
// 400-499 = client error
// 500-599 = server error responses
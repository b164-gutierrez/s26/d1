const http = require('http');

const port = 4000;


// create a variable 'server' that stores the ouput of the createServer method.

// to access the /greeting (routes) we will use the request object.

// the 'url' property refers to the url or the link or routes int the client

// http://localhost:4000/greeting

// /greeting - this is the routes that the '.url' reads.

const server = http.createServer((req, res) => {
		if(req.url =='/greeting'){
				res.writeHead(200, {'Content-Type': 'text/plain'});
				res.end('Hello Again')
		}
		 	else if(req.url == '/homepage') {
		res.writeHead(200,{'Content-Type':'text/plain'});
		res.end('This is the home page. Welcome')
	}
	else if(req.url == '/about') {
		res.writeHead(200,{'Content-Type':'text/plain'});
		res.end('Welcome to about me')
	}
	else {
		res.writeHead(404,{'Content-Type':'text/plain'});
		res.end('Page not found')
	}
})




// user the server and port variable
server.listen(port);



console.log(`Server now accesible at localhost:${port}.`)


// miniactivity

